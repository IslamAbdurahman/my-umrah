@extends('layouts.main')

@section('main-content')
    <!-- /.card -->

    <div class="card">
        <div class="card-header">
            <ul class="navbar list-unstyled m-0 p-0">
                <li>
                    <h3 class="card-title">Biz haqimizda</h3>
                </li>
                <li>
                    <!-- SEARCH FORM -->
                    <form action="{{ route('transaction.index') }}" method="get" class="form-inline m-0 ml-md-3">
                        @csrf
                        <div class="input-group input-group-sm">
                            <input name="search" value="{{ $search }}" class="form-control form-control-navbar" type="search" placeholder="Qidirish" aria-label="Search">
                            <div class="input-group-append">
                                <button class="btn btn-success" type="submit">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </li>
            </ul>
        </div>
        <!-- /.card-header -->
        <div class="card-body pt-0 table-responsive">
            <table id="example1" class="table table-bordered table-striped ">
                <thead>
                <tr>
                    <th>N</th>
                    <th>Payment</th>
                    <th>Vaqt</th>
                    <th>Transaction</th>
                    <th>Summa</th>
                    <th>Valyuta</th>
                    <th>Holat</th>
                    <th>Mijoz</th>
                </tr>
                </thead>
                <tbody>
                @foreach($transaction as $item)
                    <tr>
                        <td>{{ $loop->index+1 }}</td>
                        <td>{{ $item->created_at }}</td>
                        <td>{{ $item->payment_system }}</td>
                        <td>{{ $item->system_transaction_id }}</td>
                        <td>{{ $item->amount }}</td>
                        <td>{{ $item->currency_code }}</td>
                        <td>{{ $item->comment }}</td>
                        <td>{{ $item->name }}</td>
                    </tr>

                @endforeach


                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
@endsection
