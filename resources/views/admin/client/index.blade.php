@extends('layouts.main')

@section('main-content')
    <!-- /.card -->

    <div class="card">
        <div class="card-header">
            <ul class="navbar list-unstyled m-0 p-0">
                <li>
                    <h3 class="card-title">Biz haqimizda</h3>
                </li>
                <li>
                    <button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#about_add">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-plus" viewBox="0 0 16 16">
                            <path d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                            <path fill-rule="evenodd" d="M13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z"/>
                        </svg>
                    </button>
                    <div class="modal fade" id="about_add">
                        <div class="modal-dialog">
                            <div class="modal-content bg-primary">
                                <form action="{{ route('client.store') }}" method="post" enctype="multipart/form-data">

                                    <div class="modal-header">
                                        <h4 class="modal-title">Mijoz qo'shish</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">

                                        @csrf
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">F.I.SH</label>
                                                <input name="name" type="text" class="form-control" value="" id="exampleInputEmail1" placeholder="Ism kiriting" required>
                                            </div>

                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Telefon</label>
                                                <input name="phone_number" type="number" class="form-control" value="" id="exampleInputEmail1" placeholder="Telefon kiriting" required>
                                            </div>

                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Address</label>
                                                <input name="address" type="text" class="form-control" value="" id="exampleInputEmail1" placeholder="Address kiriting" required>
                                            </div>


                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input" id="exampleCheck1" required>
                                                <label class="form-check-label" for="exampleCheck1">Tekshirish</label>
                                            </div>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-outline-light" data-dismiss="modal">Yopish</button>
                                        <button type="submit" class="btn btn-outline-light">Saqlash</button>
                                    </div>

                                </form>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                </li>
            </ul>
        </div>
        <!-- /.card-header -->
        <div class="card-body pt-0 table-responsive">
            <table id="example1" class="table table-bordered table-striped ">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Ism</th>
                    <th>Telefon</th>
                    <th>Summa</th>
                    <th>Address</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($client as $item)
                    <tr>
                        <td class="p-1">{{ $item->id }}</td>
                        <td class="p-1"><a href="{{ route('client.show',$item->id) }}">{{ $item->name }}</a></td>
                        <td class="p-1">{{ $item->phone_number }}</td>
                        <td class="p-1">{{ $item->summa }}</td>
                        <td class="p-1">{{ $item->address }}</td>
                        <td class="p-1">
                            <button type="button" class="btn btn-success m-0 p-1 py-0 float-right" data-toggle="modal" data-target="#update{{ $item->id }}">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-plus" viewBox="0 0 16 16">
                                    <path d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                                    <path fill-rule="evenodd" d="M13.5 5a.5.5 0 0 1 .5.5V7h1.5a.5.5 0 0 1 0 1H14v1.5a.5.5 0 0 1-1 0V8h-1.5a.5.5 0 0 1 0-1H13V5.5a.5.5 0 0 1 .5-.5z"/>
                                </svg>
                            </button>
                            <div class="modal fade" id="update{{$item->id}}">
                                <div class="modal-dialog">
                                    <div class="modal-content bg-primary">
                                        <form action="{{ route('client.update',$item->id) }}" method="post" enctype="multipart/form-data">
                                            @csrf
                                            @method('put')
                                            <div class="modal-header">
                                                <h4 class="modal-title">Mijoz qo'shish</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">

                                                @csrf
                                                <div class="card-body">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">F.I.SH</label>
                                                        <input name="name" type="text" class="form-control" value="{{$item->name}}" id="exampleInputEmail1" placeholder="Ism kiriting" required>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Telefon</label>
                                                        <input name="phone_number" type="number" class="form-control" value="{{$item->phone_number}}" id="exampleInputEmail1" placeholder="Telefon kiriting" required>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Address</label>
                                                        <input name="address" type="text" class="form-control" value="{{$item->address}}" id="exampleInputEmail1" placeholder="Address kiriting" required>
                                                    </div>


                                                    <div class="form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1" required>
                                                        <label class="form-check-label" for="exampleCheck1">Tekshirish</label>
                                                    </div>
                                                </div>
                                                <!-- /.card-body -->
                                            </div>
                                            <div class="modal-footer justify-content-between">
                                                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Yopish</button>
                                                <button type="submit" class="btn btn-outline-light">Saqlash</button>
                                            </div>

                                        </form>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                        </td>
                    </tr>

                @endforeach

                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
@endsection
