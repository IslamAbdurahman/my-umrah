-- phpMyAdmin SQL Dump
-- version 5.1.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 23, 2023 at 12:47 PM
-- Server version: 8.0.30-cll-lve
-- PHP Version: 7.2.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `x_u_9627_my-umrah`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int NOT NULL,
  `head` varchar(255) NOT NULL,
  `text` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `head`, `text`) VALUES
(1, 'Biz haqimizda', 'Umra Ziyorati');

-- --------------------------------------------------------

--
-- Table structure for table `carousel`
--

CREATE TABLE `carousel` (
  `id` int NOT NULL,
  `head` varchar(255) NOT NULL,
  `text` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `carousel`
--

INSERT INTO `carousel` (`id`, `head`, `text`) VALUES
(2, 'Biz bilan sayohat qiling', 'Umra ziyorati');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone_number` int NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `name`, `phone_number`, `address`) VALUES
(1, 'Sanjarbek', 911157709, 'Fergana'),
(2, 'Nurulloxon', 331157709, 'Yaypan'),
(3, 'Salom', 911157709, 'Oltiariq district'),
(4, '+998911157709', 11222, 'Oltiariq district'),
(5, '+998911157709', 111222, 'Oltiariq district');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_05_09_041452_create_roles_table', 2),
(6, '2019_02_15_131448_create_transactions_table', 3),
(7, '2019_02_15_150408_create_payment_systems_table', 3),
(8, '2019_02_24_073925_create_payment_system_params_table', 3),
(9, '2020_03_17_105343_create_projects_table', 3),
(10, '2020_03_17_105343_update_detail_transactions_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_systems`
--

CREATE TABLE `payment_systems` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `system` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'not_active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_systems`
--

INSERT INTO `payment_systems` (`id`, `name`, `system`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Payme', 'payme', 'active', '2023-01-02 09:21:52', '2023-01-02 09:25:15'),
(2, 'Click', 'click', 'active', '2023-01-02 09:21:52', '2023-01-06 03:36:47'),
(3, 'Paynet', 'paynet', 'not_active', '2023-01-02 09:21:52', '2023-01-16 22:54:05'),
(4, 'Stripe', 'stripe', 'not_active', '2023-01-02 09:21:52', '2023-01-02 09:21:52'),
(5, 'Oson', 'oson', 'not_active', '2023-01-16 10:39:43', '2023-01-16 22:54:06'),
(6, 'Uzcard', 'uzcard', 'not_active', '2023-01-16 10:39:57', '2023-01-16 22:54:09');

-- --------------------------------------------------------

--
-- Table structure for table `payment_system_params`
--

CREATE TABLE `payment_system_params` (
  `id` int UNSIGNED NOT NULL,
  `system` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_system_params`
--

INSERT INTO `payment_system_params` (`id`, `system`, `label`, `name`, `value`, `created_at`, `updated_at`) VALUES
(9, 'paynet', 'Login', 'login', 'login', '2023-01-02 09:21:52', '2023-01-02 09:21:52'),
(10, 'paynet', 'Password', 'password', 'password', '2023-01-02 09:21:52', '2023-01-02 09:21:52'),
(11, 'paynet', 'Service id', 'service_id', 'service_id', '2023-01-02 09:21:52', '2023-01-02 09:21:52'),
(12, 'stripe', 'Secret key', 'secret_key', 'secret_key', '2023-01-02 09:21:52', '2023-01-02 09:21:52'),
(13, 'stripe', 'Publishable key', 'publishable_key', 'publishable_key', '2023-01-02 09:21:52', '2023-01-02 09:21:52'),
(14, 'stripe', 'Proxy', 'proxy', '', '2023-01-02 09:21:52', '2023-01-02 09:21:52'),
(51, 'click', 'Service id', 'service_id', '26014', '2023-01-10 03:32:22', '2023-01-10 03:32:22'),
(52, 'click', 'Secret key', 'secret_key', 'CHDDwGuiUZQbrs', '2023-01-10 03:32:22', '2023-01-10 03:32:22'),
(53, 'click', 'Merchant Id', 'merchant_id', '18296', '2023-01-10 03:32:22', '2023-01-10 03:32:22'),
(54, 'click', 'Merchant user id', 'merchant_user_id', '29522', '2023-01-10 03:32:22', '2023-01-10 03:32:22'),
(59, 'oson', NULL, NULL, NULL, '2023-01-16 10:40:44', '2023-01-16 10:40:44'),
(60, 'oson', NULL, NULL, NULL, '2023-01-16 10:40:44', '2023-01-16 10:40:44'),
(61, 'oson', NULL, NULL, NULL, '2023-01-16 10:40:44', '2023-01-16 10:40:44'),
(62, 'oson', NULL, NULL, NULL, '2023-01-16 10:40:44', '2023-01-16 10:40:44'),
(67, 'payme', 'Login', 'login', 'Paycom', '2023-01-16 10:43:45', '2023-01-16 10:43:45'),
(68, 'payme', 'Merchant id', 'merchant_id', '638e02d00c4c67ea0602f41c', '2023-01-16 10:43:45', '2023-01-16 10:43:45'),
(69, 'payme', 'Password', 'password', 'csCHJrve17Oi6efq5#WfScHv5jIA3CYj&zog', '2023-01-16 10:43:45', '2023-01-16 10:43:45'),
(70, 'payme', 'Key', 'key', 'key', '2023-01-16 10:43:45', '2023-01-16 10:43:45');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int NOT NULL,
  `details` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(0, 'Haridor'),
(1, 'Super Admin'),
(2, 'Admin'),
(3, 'Manager');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int UNSIGNED NOT NULL,
  `payment_system` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `system_transaction_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double(15,5) NOT NULL,
  `currency_code` int NOT NULL,
  `state` int NOT NULL,
  `updated_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transactionable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transactionable_id` int DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `detail` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `payment_system`, `system_transaction_id`, `amount`, `currency_code`, `state`, `updated_time`, `comment`, `transactionable_type`, `transactionable_id`, `deleted_at`, `created_at`, `updated_at`, `detail`) VALUES
(1, 'click', '1980643677', 2000.00000, 860, 2, '1673340646291', 'Success', 'App\\Models\\User', 1, NULL, '2023-01-10 03:50:46', '2023-01-10 03:50:46', '{\"create_time\":1673340646291,\"system_time_datetime\":\"2023-01-10 13:50:45\"}'),
(2, 'click', '1980741843', 1100.00000, 860, 2, '1673344773398', 'Success', 'App\\Models\\User', 1, NULL, '2023-01-10 04:59:33', '2023-01-10 04:59:34', '{\"create_time\":1673344773398,\"system_time_datetime\":\"2023-01-10 14:59:21\"}'),
(3, 'click', '1982469325', 2000.00000, 860, 2, '1673446207385', 'Success', 'App\\Models\\User', 1, NULL, '2023-01-11 09:10:07', '2023-01-11 09:10:08', '{\"create_time\":1673446207385,\"system_time_datetime\":\"2023-01-11 19:10:06\"}'),
(4, 'click', '1982537118', 2000.00000, 860, 2, '1673449329010', 'Success', 'App\\Models\\Client', 1, NULL, '2023-01-11 10:02:09', '2023-01-11 10:02:09', '{\"create_time\":1673449329010,\"system_time_datetime\":\"2023-01-11 20:02:08\"}'),
(5, 'click', '1982557250', 2000.00000, 860, 2, '1673450297085', 'Success', 'App\\Models\\Client', 2, NULL, '2023-01-11 10:18:17', '2023-01-11 10:18:17', '{\"create_time\":1673450297085,\"system_time_datetime\":\"2023-01-11 20:18:16\"}'),
(6, 'click', '1982579947', 2000.00000, 860, 2, '1673451280876', 'Success', 'App\\Models\\Client', 2, NULL, '2023-01-11 10:34:40', '2023-01-11 10:34:41', '{\"create_time\":1673451280876,\"system_time_datetime\":\"2023-01-11 20:34:35\"}'),
(7, 'click', '1984365133', 2000.00000, 860, -1, '1673592268000', 'Success', 'App\\Models\\Client', 1, NULL, '2023-01-13 01:44:28', '2023-01-13 01:46:07', '{\"create_time\":1673592268000,\"system_time_datetime\":\"2023-01-13 11:44:27\"}'),
(8, 'click', '1984365811', 2000.00000, 860, 2, '1673592291579', 'Success', 'App\\Models\\Client', 1, NULL, '2023-01-13 01:44:51', '2023-01-13 01:44:52', '{\"create_time\":1673592291579,\"system_time_datetime\":\"2023-01-13 11:44:51\"}'),
(9, 'click', '1984371101', 1500.00000, 860, 2, '1673592479484', 'Success', 'App\\Models\\Client', 1, NULL, '2023-01-13 01:47:59', '2023-01-13 01:48:00', '{\"create_time\":1673592479484,\"system_time_datetime\":\"2023-01-13 11:47:58\"}');

-- --------------------------------------------------------

--
-- Table structure for table `trips`
--

CREATE TABLE `trips` (
  `id` int NOT NULL,
  `country` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `text` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `trips`
--

INSERT INTO `trips` (`id`, `country`, `city`, `price`, `img`, `text`) VALUES
(5, 'Uzbekistan', 'Fergana', '50', '16771377531972.jpg', 'Biz siz bilan birga!!'),
(6, 'Turkiya', 'Antaliya', '250', '16771378056076.jpg', 'Biz siz bilan birga!!');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(264) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'noimage',
  `role` int DEFAULT '0',
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `image`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Islombek', 'onlymarch567@gmail.com', NULL, '$2y$10$X0iXzl.6D0R2NJLBqmNsOOk8OiVjFtzBZFRC2QZkuIuGTBpegPgka', '16771378413006.jpg', 1, 'ZqTJqYETE4xZCi3oJ60xIZmqva5Vd0f3SrJJWYMsmg9Wp8mo5jhFY2NmzDxq', '2022-05-05 06:04:17', '2023-02-23 02:37:21'),
(2, 'Bekzod', 'ali@gmail.com', NULL, '$2y$10$fgDgb8CVHu5tfID.NYkBturZ4Gl5nenWGDxzcso5xoa2fXf2eF6E2', '16533023782688.jpg', 2, 'UjPfbVa0FWcrTVF1XY1MfJnf0g4r0neu8FMcTP1DP5LaoBmUZJlfmsRdJAp8', '2022-05-05 13:01:20', '2022-05-23 05:39:38'),
(3, 'Ziyodulla', 'vali@gmail.com', NULL, '$2y$10$bPUnetzSb3PXgykCIjDdE.dvrux9H/F7O28/Z/k6eTPLynriuatSW', '16533023958667.jpg', 3, 'sXdZ6OIVfsoupZul7dM84UOxRQ8QMPWFQA1W0lTNxHRPVrX1vpjNPBgmZgHi', '2022-05-06 00:32:05', '2022-05-23 05:39:55'),
(4, 'Azizov domla', 'abdurahmanislam304@gmail.com', NULL, '$2y$10$qCucnsnVfNQe8fYukF.O8u7fhlqsFWsQuyY/u935v/TGdaxsQqadC', '16533024112289.jpg', 3, NULL, '2022-05-06 11:12:40', '2022-05-23 05:40:11'),
(5, 'gani', 'gani@gmail.com', NULL, '$2y$10$xFuQ3sZrPdIEf2s7sjo.nOetphqi7gqKgHaVx338gQNM38bc7YMEO', '16533025198521.jpg', 0, NULL, '2022-05-23 05:41:59', '2022-05-23 05:41:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carousel`
--
ALTER TABLE `carousel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_systems`
--
ALTER TABLE `payment_systems`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `payment_systems_system_unique` (`system`);

--
-- Indexes for table `payment_system_params`
--
ALTER TABLE `payment_system_params`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trips`
--
ALTER TABLE `trips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `carousel`
--
ALTER TABLE `carousel`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `payment_systems`
--
ALTER TABLE `payment_systems`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `payment_system_params`
--
ALTER TABLE `payment_system_params`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `trips`
--
ALTER TABLE `trips`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
