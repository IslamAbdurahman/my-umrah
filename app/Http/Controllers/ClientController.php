<?php

namespace App\Http\Controllers;

use App\Models\Client;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->search){

            $client = DB::table('clients as c')
            ->leftJoin('transactions as t','c.id','=','t.transactionable_id')
            ->select('c.*',DB::raw('SUM(t.amount) as summa'))
            ->groupBy('c.id')
            ->get();

            $search = $request->search;
        }else{

            $client = DB::table('clients as c')
                ->leftJoin('transactions as t','c.id','=','t.transactionable_id')
                ->select('c.*',DB::raw('SUM(t.amount) as summa'))
                ->groupBy('c.id')
                ->get();


            $search = '';
        }

        return view('admin.client.index', compact('client','search'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'phone_number'=>'required',
            'address'=>'required',
        ]);
        $client = new Client;
        $client->name = $request->name;
        $client->phone_number = $request->phone_number;
        $client->address = $request->address;
        $client->save();


        return redirect()->route('client.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
            $transaction = \Illuminate\Support\Facades\DB::select('select t.*, c.name from transactions t
            join clients c on t.transactionable_id = c.id where c.id='.$id);



        return view('admin.client.show', compact('transaction'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            'phone_number'=>'required',
            'address'=>'required',
        ]);
        $client = Client::find($id);
        $client->name = $request->name;
        $client->phone_number = $request->phone_number;
        $client->address = $request->address;
        $client->update();


        return redirect()->route('client.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
